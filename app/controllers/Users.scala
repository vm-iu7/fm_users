package controllers

import javax.inject.Inject

import models.UserDAO
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.Future


class Users @Inject() (
  private val userDAO: UserDAO
) extends Controller {

  def get(id: Int) = Action.async {
    userDAO.lookup(id).map { optUser =>
      optUser
        .map(u => Ok(Json.toJson(u)))
        .getOrElse(NotFound(Json.obj("error" -> s"User with id = $id Not found")))
    }
  }

  def getByEmail(email: String) = Action.async {
    userDAO.getByEmail(email).map { optUser =>
      optUser
        .map(u => Ok(Json.toJson(u)))
        .getOrElse(NotFound(Json.obj("error" -> s"User with email $email not found")))
    }
  }


  private case class UserCreateParams(
    email: String,
    firstName: String,
    lastName: String,
    password: String
  )

  private implicit val userCreateParamsJsonFormat = Json.format[UserCreateParams]

  def create() = Action.async(parse.json) { request =>
    val optUser = request.body.asOpt[UserCreateParams]
    optUser.map { user =>
      userDAO.create(user.email, user.firstName, user.lastName).map { createdUser =>
        Ok(Json.toJson(createdUser))
      }
    }.getOrElse {
      Future(BadRequest(Json.obj("error" -> "error parsing user parameters")))
    }
  }

}
