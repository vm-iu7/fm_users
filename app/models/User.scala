package models

import javax.inject.{Inject, Singleton}

import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfig}
import play.api.libs.json.Json
import slick.driver.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}


case class User(id: Int, email: String, firstName: String, lastName: String)

object User {
  implicit val jsonFormat = Json.format[User]
}

@Singleton
class UserDAO @Inject() (
  protected val dbConfigProvider: DatabaseConfigProvider
) extends HasDatabaseConfig[JdbcProfile] {

  import driver.api._

  protected val dbConfig = dbConfigProvider.get[JdbcProfile]

  private val usersTable = TableQuery[UsersTable]

  def lookup(id: Int)(implicit ec: ExecutionContext): Future[Option[User]] = {
    dbConfig.db.run {
      usersTable.filter(u => u.id === id).result.headOption
    }
  }

  def getByEmail(email: String)(implicit ec: ExecutionContext): Future[Option[User]] = {
    dbConfig.db.run {
      usersTable.filter(u => u.email === email).result.headOption
    }
  }

  def create(email: String, firstName: String, lastName: String)
            (implicit ec: ExecutionContext): Future[User] = {
    val user = User(0, email, firstName, lastName)
    dbConfig.db.run {
      (usersTable returning usersTable.map(u => u.id)) += user
    }.map { id =>
      user.copy(id = id)
    }
  }

  private class UsersTable(tag: Tag) extends Table[User](tag, "USERS") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def email = column[String]("email")
    def firstName = column[String]("firstName")
    def lastName = column[String]("lastName")

    def * = (id, email, firstName, lastName) <> ((User.apply _).tupled, User.unapply)
  }

}
