USE fm_users;

# --- !Ups
create table USERS (
  id int AUTO_INCREMENT not null,
  email varchar(255) not null,
  password_hash char(255) not null,
  firstName varchar(255) not null,
  lastName varchar(255) not null,

  primary key(id),
  constraint uc_user_email unique (email)
);

alter table USERS AUTO_INCREMENT=1000000000;
create index idx_users_email on USERS(email);

# --- !Downs
drop table USERS;
